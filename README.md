### Prerequisites

* Basic knowledge of Electronics
* Intermediate expertise of PCB designing using Eagle

### Course Content

#### Day 1

* Introduction to IPC standard
* Understanding Eagle DRC tool
* Customizing the DRC
* Create your work bench
* Spice application with Eagle

#### Day 2

* Different types of PCB Design
* Design techniques for Power, Audio and High Speed
* Common errors that causes a properly designed PCB to fail in production
* How to use Eagle to overcome common errors

#### Day 3

* Power Supply based PCB Design guidelines
* Thermal isolation
* Switching Driver routing
* PCB Heat sink
* Ground isolation Kelvin connection
* Electrical Isolation as per EN61000 standard

#### Day 4

* Audio based PCB Design guidelines
* Isolating Noisy Line with the help of DRC
* Op-Amp Termination in PCB
* DAC and ADC routing
* Differential pair routing techniques

#### Day 5

* High Speed PCB Design guidelines
* Impedence
* Parasitic Inductance, Capacitance, Resistance
* Tear Drop via generation in Eagle
